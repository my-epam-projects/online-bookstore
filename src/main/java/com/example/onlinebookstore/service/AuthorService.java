package com.example.onlinebookstore.service;

import com.example.onlinebookstore.model.Author;

import java.util.List;

public interface AuthorService {

    List<Author> getAllAuthors();

    Author getAuthorById(Long id);

    Author saveAuthor(Author author);

    void deleteAuthor(Long id);

    List<Author> searchAuthors(String keyword);
}
