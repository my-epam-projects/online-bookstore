package com.example.onlinebookstore.service;

import com.example.onlinebookstore.model.Genre;

import java.util.List;

public interface GenreService {

    List<Genre> getAllGenres();

    Genre getGenreById(Long id);

    Genre saveGenre(Genre genre);

    void deleteGenre(Long id);

    List<Genre> searchGenres(String keyword);
}
