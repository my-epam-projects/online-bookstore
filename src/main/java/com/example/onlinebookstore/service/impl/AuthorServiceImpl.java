package com.example.onlinebookstore.service.impl;

import com.example.onlinebookstore.model.Author;
import com.example.onlinebookstore.repository.AuthorRepository;
import com.example.onlinebookstore.service.AuthorService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
public class AuthorServiceImpl implements AuthorService {

    private final AuthorRepository repository;

    @Override
    public List<Author> getAllAuthors() {
        return repository.findAll();
    }

    @Override
    public Author getAuthorById(Long id) {
        return repository.findById(id).orElse(null);
    }

    @Override
    public Author saveAuthor(Author author) {
        return repository.save(author);
    }

    @Override
    public void deleteAuthor(Long id) {
        repository.deleteById(id);
    }

    @Override
    public List<Author> searchAuthors(String keyword) {
        return repository.findByNameContainingIgnoreCase(keyword);
    }
}
