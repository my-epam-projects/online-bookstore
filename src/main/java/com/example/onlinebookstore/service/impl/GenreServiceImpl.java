package com.example.onlinebookstore.service.impl;

import com.example.onlinebookstore.model.Genre;
import com.example.onlinebookstore.repository.GenreRepository;
import com.example.onlinebookstore.service.GenreService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
public class GenreServiceImpl implements GenreService {

    private final GenreRepository repository;


    @Override
    public List<Genre> getAllGenres() {
        return repository.findAll();
    }

    @Override
    public Genre getGenreById(Long id) {
        return repository.findById(id).orElse(null);
    }

    @Override
    public Genre saveGenre(Genre genre) {
        return repository.save(genre);
    }

    @Override
    public void deleteGenre(Long id) {
        repository.deleteById(id);
    }

    @Override
    public List<Genre> searchGenres(String keyword) {
        return repository.findByNameContainingIgnoreCase(keyword);
    }
}
