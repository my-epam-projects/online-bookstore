package com.example.onlinebookstore.controller;

import com.example.onlinebookstore.model.Author;
import com.example.onlinebookstore.service.AuthorService;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import java.util.Arrays;
import java.util.List;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@WebMvcTest(AuthorController.class)
class AuthorControllerTest {


    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private AuthorService authorService;

    @Autowired
    private ObjectMapper objectMapper;

    @Test
    void testGetAllAuthors() throws Exception {
        // Mocking service
        List<Author> authors = Arrays.asList(new Author(1L, "Author1"), new Author(2L, "Author2"));
        when(authorService.getAllAuthors()).thenReturn(authors);

        // Performing GET request
        mockMvc.perform(MockMvcRequestBuilders.get("/api/authors"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$[0].name").value("Author1"));
    }

    @Test
    void testGetAuthorById() throws Exception {
        // Mocking service
        Author author = new Author(1L, "Author1");
        when(authorService.getAuthorById(1L)).thenReturn(author);

        // Performing GET request
        mockMvc.perform(MockMvcRequestBuilders.get("/api/authors/1"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.name").value("Author1"));
    }

    @Test
    void testSaveAuthor() throws Exception {
        // Mocking service
        Author authorToSave = new Author(null, "NewAuthor");
        Author savedAuthor = new Author(3L, "NewAuthor");
        when(authorService.saveAuthor(any(Author.class))).thenReturn(savedAuthor);

        // Performing POST request
        mockMvc.perform(MockMvcRequestBuilders.post("/api/authors")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsString(authorToSave)))
                .andExpect(status().isCreated())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.name").value("NewAuthor"));

        verify(authorService, times(1)).saveAuthor(any(Author.class));
    }

    @Test
    void testUpdateAuthor() throws Exception {
        // Mocking service
        Author updatedAuthor = new Author(1L, "UpdatedAuthor");
        when(authorService.saveAuthor(any(Author.class))).thenReturn(updatedAuthor);

        // Performing PUT request
        mockMvc.perform(MockMvcRequestBuilders.put("/api/authors/1")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsString(updatedAuthor)))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.name").value("UpdatedAuthor"));

        verify(authorService, times(1)).saveAuthor(any(Author.class));
    }

    @Test
    void testDeleteAuthor() throws Exception {
        // Performing DELETE request
        mockMvc.perform(MockMvcRequestBuilders.delete("/api/authors/1"))
                .andExpect(status().isNoContent());

        verify(authorService, times(1)).deleteAuthor(1L);
    }

    @Test
    void testSearchAuthors() throws Exception {
        // Mocking service
        List<Author> searchedAuthors = List.of(new Author(1L, "Author1"));
        when(authorService.searchAuthors("Author")).thenReturn(searchedAuthors);

        // Performing GET request for search
        mockMvc.perform(MockMvcRequestBuilders.get("/api/authors/search?keyword=Author"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$[0].name").value("Author1"));
    }
}
