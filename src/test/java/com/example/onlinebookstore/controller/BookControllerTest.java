package com.example.onlinebookstore.controller;

import com.example.onlinebookstore.model.Author;
import com.example.onlinebookstore.model.Book;
import com.example.onlinebookstore.model.Genre;
import com.example.onlinebookstore.service.BookService;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import java.util.List;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;


@WebMvcTest(BookController.class)
class BookControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private BookService bookService;

    @Autowired
    private ObjectMapper objectMapper;

    @Test
    void testGetAllBooks() throws Exception {
        // Mocking service
        List<Book> books = List.of(new Book(1L, "Book1", 19.99, 100, new Author(1L, "Author1"), new Genre(1L, "Genre1")));
        when(bookService.getAllBooks()).thenReturn(books);

        // Performing GET request
        mockMvc.perform(MockMvcRequestBuilders.get("/api/books"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$[0].title").value("Book1"));
    }


    @Test
    void testGetBookById() throws Exception {
        // Mocking service
        Book book = new Book(1L, "Book1", 19.99, 100, new Author(1L, "Author1"), new Genre(1L, "Genre1"));
        when(bookService.getBookById(1L)).thenReturn(book);

        // Performing GET request
        mockMvc.perform(MockMvcRequestBuilders.get("/api/books/1"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.title").value("Book1"));
    }


    @Test
    void testSaveBook() throws Exception {
        // Mocking service
        Book bookToSave = new Book(null, "NewBook", 29.99, 50, new Author(2L, "Author2"), new Genre(2L, "Genre2"));
        Book savedBook = new Book(2L, "NewBook", 29.99, 50, new Author(2L, "Author2"), new Genre(2L, "Genre2"));
        when(bookService.saveBook(any(Book.class))).thenReturn(savedBook);

        // Performing POST request
        mockMvc.perform(post("/api/books")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsString(bookToSave)))
                .andExpect(status().isCreated())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.title").value("NewBook"));

        verify(bookService, times(1)).saveBook(any(Book.class));
    }

    @Test
    void testUpdateBook() throws Exception {
        // Mocking service
        Book updatedBook = new Book(1L, "UpdatedBook", 24.99, 80, new Author(1L, "Author1"), new Genre(1L, "Genre1"));
        when(bookService.saveBook(any(Book.class))).thenReturn(updatedBook);

        // Performing PUT request
        mockMvc.perform(put("/api/books/1")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsString(updatedBook)))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.title").value("UpdatedBook"));

        verify(bookService, times(1)).saveBook(any(Book.class));
    }

    @Test
    void testDeleteBook() throws Exception {
        // Performing DELETE request
        mockMvc.perform(MockMvcRequestBuilders.delete("/api/books/1"))
                .andExpect(status().isNoContent());
    }

    @Test
    void testSearchBooks() throws Exception {
        // Mocking service
        List<Book> searchedBooks = List.of(new Book(1L, "Book1", 19.99, 100, new Author(1L, "Author1"), new Genre(1L, "Genre1")));
        when(bookService.searchBooks("Book")).thenReturn(searchedBooks);

        // Performing GET request for search
        mockMvc.perform(MockMvcRequestBuilders.get("/api/books/search?keyword=Book"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$[0].title").value("Book1"));
    }
}
