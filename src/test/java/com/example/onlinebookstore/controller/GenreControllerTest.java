package com.example.onlinebookstore.controller;

import com.example.onlinebookstore.model.Genre;
import com.example.onlinebookstore.service.GenreService;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import java.util.Arrays;
import java.util.List;

import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@WebMvcTest(GenreController.class)
 class GenreControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private GenreService genreService;

    @Autowired
    private ObjectMapper objectMapper;

    @Test
    void testGetAllGenres() throws Exception {
        // Mocking service
        List<Genre> genres = Arrays.asList(new Genre(1L, "Genre1"), new Genre(2L, "Genre2"));
        when(genreService.getAllGenres()).thenReturn(genres);

        // Performing GET request
        mockMvc.perform(MockMvcRequestBuilders.get("/api/genres"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$[0].name").value("Genre1"));
    }

    @Test
    void testGetGenreById() throws Exception {
        // Mocking service
        Genre genre = new Genre(1L, "Genre1");
        when(genreService.getGenreById(1L)).thenReturn(genre);

        // Performing GET request
        mockMvc.perform(MockMvcRequestBuilders.get("/api/genres/1"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.name").value("Genre1"));
    }

    @Test
    void testSaveGenre() throws Exception {
        // Mocking service
        Genre genreToSave = new Genre(null, "NewGenre");
        Genre savedGenre = new Genre(3L, "NewGenre");
        when(genreService.saveGenre(any(Genre.class))).thenReturn(savedGenre);

        // Performing POST request
        mockMvc.perform(MockMvcRequestBuilders.post("/api/genres")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsString(genreToSave)))
                .andExpect(status().isCreated())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.name").value("NewGenre"));

        verify(genreService, times(1)).saveGenre(any(Genre.class));
    }

    @Test
    void testUpdateGenre() throws Exception {
        // Mocking service
        Genre updatedGenre = new Genre(1L, "UpdatedGenre");
        when(genreService.saveGenre(any(Genre.class))).thenReturn(updatedGenre);

        // Performing PUT request
        mockMvc.perform(MockMvcRequestBuilders.put("/api/genres/1")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsString(updatedGenre)))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.name").value("UpdatedGenre"));

        verify(genreService, times(1)).saveGenre(any(Genre.class));
    }

    @Test
    void testDeleteGenre() throws Exception {
        // Performing DELETE request
        mockMvc.perform(MockMvcRequestBuilders.delete("/api/genres/1"))
                .andExpect(status().isNoContent());

        verify(genreService, times(1)).deleteGenre(1L);
    }

    @Test
    void testSearchGenres() throws Exception {
        // Mocking service
        List<Genre> searchedGenres = List.of(new Genre(1L, "Genre1"));
        when(genreService.searchGenres("Genre")).thenReturn(searchedGenres);

        // Performing GET request for search
        mockMvc.perform(MockMvcRequestBuilders.get("/api/genres/search?keyword=Genre"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$[0].name").value("Genre1"));
    }
}
