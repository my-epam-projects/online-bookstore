package com.example.onlinebookstore.service;

import com.example.onlinebookstore.model.Author;
import com.example.onlinebookstore.repository.AuthorRepository;
import com.example.onlinebookstore.service.impl.AuthorServiceImpl;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.*;

@SpringBootTest
 class AuthorServiceTest {

    @Mock
    private AuthorRepository authorRepository;

    @InjectMocks
    private AuthorServiceImpl authorService;

    @Test
    void testGetAllAuthors() {
        // Mocking repository
        List<Author> authors = Arrays.asList(new Author(1L, "Author1"), new Author(2L, "Author2"));
        when(authorRepository.findAll()).thenReturn(authors);

        // Calling service method
        List<Author> result = authorService.getAllAuthors();

        // Assertions
        assertEquals(2, result.size());
        assertEquals("Author1", result.get(0).getName());
        assertEquals("Author2", result.get(1).getName());

        // Verify repository method is called
        verify(authorRepository, times(1)).findAll();
    }

    @Test
    void testGetAuthorById() {
        // Mocking repository
        Author author = new Author(1L, "Author1");
        when(authorRepository.findById(1L)).thenReturn(Optional.of(author));

        // Calling service method
        Author result = authorService.getAuthorById(1L);

        // Assertions
        assertEquals("Author1", result.getName());

        // Verify repository method is called
        verify(authorRepository, times(1)).findById(1L);
    }

    @Test
    void testSaveAuthor() {
        // Mocking repository
        Author authorToSave = new Author(null, "NewAuthor");
        Author savedAuthor = new Author(3L, "NewAuthor");
        when(authorRepository.save(authorToSave)).thenReturn(savedAuthor);

        // Calling service method
        Author result = authorService.saveAuthor(authorToSave);

        // Assertions
        assertEquals(3L, result.getId());
        assertEquals("NewAuthor", result.getName());

        // Verify repository method is called
        verify(authorRepository, times(1)).save(authorToSave);
    }

    @Test
    void testDeleteAuthor() {
        // Calling service method
        authorService.deleteAuthor(1L);

        // Verify repository method is called
        verify(authorRepository, times(1)).deleteById(1L);
    }

    @Test
    void testSearchAuthors() {
        // Mocking repository
        List<Author> searchedAuthors = List.of(new Author(1L, "Author1"));
        when(authorRepository.findByNameContainingIgnoreCase("Author")).thenReturn(searchedAuthors);

        // Calling service method
        List<Author> result = authorService.searchAuthors("Author");

        // Assertions
        assertEquals(1, result.size());
        assertEquals("Author1", result.get(0).getName());

        // Verify repository method is called
        verify(authorRepository, times(1)).findByNameContainingIgnoreCase("Author");
    }
}
