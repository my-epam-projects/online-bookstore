package com.example.onlinebookstore.service;

import com.example.onlinebookstore.model.Author;
import com.example.onlinebookstore.model.Book;
import com.example.onlinebookstore.model.Genre;
import com.example.onlinebookstore.repository.BookRepository;
import com.example.onlinebookstore.service.impl.BookServiceImpl;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.*;

@SpringBootTest
 class BookServiceTest {

    @Mock
    private BookRepository bookRepository;

    @InjectMocks
    private BookServiceImpl bookService;

    @Test
    void testGetAllBooks() {
        // Mocking repository
        List<Book> books = Arrays.asList(
                new Book(1L, "Book1", 19.99, 100, new Author(1L, "Author1"), new Genre(1L, "Genre1")),
                new Book(2L, "Book2", 24.99, 80, new Author(2L, "Author2"), new Genre(2L, "Genre2"))
        );
        when(bookRepository.findAll()).thenReturn(books);

        // Calling service method
        List<Book> result = bookService.getAllBooks();

        // Assertions
        assertEquals(2, result.size());
        assertEquals("Book1", result.get(0).getTitle());
        assertEquals("Book2", result.get(1).getTitle());

        // Verify repository method is called
        verify(bookRepository, times(1)).findAll();
    }

    @Test
    void testGetBookById() {
        // Mocking repository
        Book book = new Book(1L, "Book1", 19.99, 100, new Author(1L, "Author1"), new Genre(1L, "Genre1"));
        when(bookRepository.findById(1L)).thenReturn(Optional.of(book));

        // Calling service method
        Book result = bookService.getBookById(1L);

        // Assertions
        assertEquals("Book1", result.getTitle());

        // Verify repository method is called
        verify(bookRepository, times(1)).findById(1L);
    }

    @Test
    void testSaveBook() {
        // Mocking repository
        Book bookToSave = new Book(null, "NewBook", 29.99, 50, new Author(2L, "Author2"), new Genre(2L, "Genre2"));
        Book savedBook = new Book(3L, "NewBook", 29.99, 50, new Author(2L, "Author2"), new Genre(2L, "Genre2"));
        when(bookRepository.save(bookToSave)).thenReturn(savedBook);

        // Calling service method
        Book result = bookService.saveBook(bookToSave);

        // Assertions
        assertEquals(3L, result.getId());
        assertEquals("NewBook", result.getTitle());

        // Verify repository method is called
        verify(bookRepository, times(1)).save(bookToSave);
    }

    @Test
    void testDeleteBook() {
        // Calling service method
        bookService.deleteBook(1L);

        // Verify repository method is called
        verify(bookRepository, times(1)).deleteById(1L);
    }


    @Test
    void testSearchBooks() {
        // Mocking repository
        List<Book> searchedBooks = List.of(
                new Book(1L, "Book1", 19.99, 100, new Author(1L, "Author1"), new Genre(1L, "Genre1"))
        );

        when(bookRepository.findByTitleContainingIgnoreCase(anyString())).thenReturn(searchedBooks);
        when(bookRepository.findByAuthorNameContainingIgnoreCase(anyString())).thenReturn(searchedBooks);
        when(bookRepository.findByGenreNameContainingIgnoreCase(anyString())).thenReturn(searchedBooks);

        // Calling service methods
        List<Book> resultTitle = bookService.searchBooks("Book");
        List<Book> resultAuthor = bookService.searchBooks("Author");
        List<Book> resultGenre = bookService.searchBooks("Genre");

        // Assertions
        assertEquals(1, resultTitle.size());
        assertEquals("Book1", resultTitle.get(0).getTitle());

        assertEquals(1, resultAuthor.size());
        assertEquals("Book1", resultAuthor.get(0).getTitle());

        assertEquals(1, resultGenre.size());
        assertEquals("Book1", resultGenre.get(0).getTitle());

        // Verify repository methods are called
        verify(bookRepository, times(1)).findByTitleContainingIgnoreCase("Book");
    }
}
