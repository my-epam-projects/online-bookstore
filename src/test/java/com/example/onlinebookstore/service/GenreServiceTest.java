package com.example.onlinebookstore.service;

import com.example.onlinebookstore.model.Genre;
import com.example.onlinebookstore.repository.GenreRepository;
import com.example.onlinebookstore.service.impl.GenreServiceImpl;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.*;

@SpringBootTest
class GenreServiceTest {

    @Mock
    private GenreRepository genreRepository;

    @InjectMocks
    private GenreServiceImpl genreService;

    @Test
    void testGetAllGenres() {
        // Mocking repository
        List<Genre> genres = Arrays.asList(new Genre(1L, "Genre1"), new Genre(2L, "Genre2"));
        when(genreRepository.findAll()).thenReturn(genres);

        // Calling service method
        List<Genre> result = genreService.getAllGenres();

        // Assertions
        assertEquals(2, result.size());
        assertEquals("Genre1", result.get(0).getName());
        assertEquals("Genre2", result.get(1).getName());

        // Verify repository method is called
        verify(genreRepository, times(1)).findAll();
    }

    @Test
    void testGetGenreById() {
        // Mocking repository
        Genre genre = new Genre(1L, "Genre1");
        when(genreRepository.findById(1L)).thenReturn(Optional.of(genre));

        // Calling service method
        Genre result = genreService.getGenreById(1L);

        // Assertions
        assertEquals("Genre1", result.getName());

        // Verify repository method is called
        verify(genreRepository, times(1)).findById(1L);
    }

    @Test
    void testSaveGenre() {
        // Mocking repository
        Genre genreToSave = new Genre(null, "NewGenre");
        Genre savedGenre = new Genre(3L, "NewGenre");
        when(genreRepository.save(genreToSave)).thenReturn(savedGenre);

        // Calling service method
        Genre result = genreService.saveGenre(genreToSave);

        // Assertions
        assertEquals(3L, result.getId());
        assertEquals("NewGenre", result.getName());

        // Verify repository method is called
        verify(genreRepository, times(1)).save(genreToSave);
    }

    @Test
    void testDeleteGenre() {
        // Calling service method
        genreService.deleteGenre(1L);

        // Verify repository method is called
        verify(genreRepository, times(1)).deleteById(1L);
    }

    @Test
    void testSearchGenres() {
        // Mocking repository
        List<Genre> searchedGenres = List.of(new Genre(1L, "Genre1"));
        when(genreRepository.findByNameContainingIgnoreCase("Genre")).thenReturn(searchedGenres);

        // Calling service method
        List<Genre> result = genreService.searchGenres("Genre");

        // Assertions
        assertEquals(1, result.size());
        assertEquals("Genre1", result.get(0).getName());

        // Verify repository method is called
        verify(genreRepository, times(1)).findByNameContainingIgnoreCase("Genre");
    }
}
